FROM bitnami/minideb-extras:jessie-r24
LABEL maintainer "Bitnami <containers@bitnami.com>"

ENV APACHE_HTTPS_PORT_NUMBER="443" \
    APACHE_HTTP_PORT_NUMBER="80" \
    BITNAMI_APP_NAME="phabricator" \
    BITNAMI_IMAGE_VERSION="2017.42.0-r0" \
    MARIADB_HOST="mariadb" \
    MARIADB_PASSWORD="" \
    MARIADB_PORT_NUMBER="3306" \
    MARIADB_USER="root" \
    PATH="/opt/bitnami/apache/bin:/opt/bitnami/php/bin:/opt/bitnami/mysql/bin:/opt/bitnami/git/bin:/opt/bitnami/arcanist/bin:/opt/bitnami/phabricator/bin:$PATH" \
    PHABRICATOR_EMAIL="user@example.com" \
    PHABRICATOR_FIRSTNAME="FirstName" \
    PHABRICATOR_HOST="127.0.0.1" \
    PHABRICATOR_LASTNAME="LastName" \
    PHABRICATOR_PASSWORD="bitnami1" \
    PHABRICATOR_SSH_PORT_NUMBER="22" \
    PHABRICATOR_SSH_USER="git" \
    PHABRICATOR_USERNAME="user"

# Update package repository
RUN apt-get update

# Install required system packages and dependencies
RUN install_packages libapr1 libaprutil1 libbz2-1.0 libc6 libcomerr2 libcurl3 libexpat1 libffi6 libfreetype6 libgcc1 libgcrypt20 libgmp10 libgnutls-deb0-28 libgpg-error0 libgssapi-krb5-2 libhogweed2 libicu52 libidn11 libjpeg62-turbo libk5crypto3 libkeyutils1 libkrb5-3 libkrb5support0 libldap-2.4-2 liblzma5 libmcrypt4 libncurses5 libnettle4 libp11-kit0 libpcre3 libpng12-0 libpq5 libreadline6 librtmp1 libsasl2-2 libssh2-1 libssl1.0.0 libstdc++6 libsybdb5 libtasn1-6 libtidy-0.99-0 libtinfo5 libuuid1 libxml2 libxslt1.1 openssh-client zlib1g openssh-server  python-pygments

RUN bitnami-pkg unpack apache-2.4.29-1 --checksum 42114e87aafb1d519ab33451b6836873bca125d78ce7423c5f7f1de4a7198596
RUN bitnami-pkg unpack php-7.1.11-0 --checksum 2dbdbadb1a9020dfced88a368371781481c27d99956eec5d6b06ce27e796c16e
RUN bitnami-pkg install mysql-client-10.1.25-0 --checksum 513ef36ab1efa5570332547c2027ae29886fe4bb56472de11ca083423a3fe366
RUN bitnami-pkg install libphp-7.1.11-0 --checksum 9acea15fe83658d792dc6b272f406a155ec7ef2f91f61c2621a2d4213fc86f9d
RUN bitnami-pkg install git-2.16.2-1 --checksum f57cd131e9576fed35d40a069e05b8fb26ec9516f37f50a05be64acb02a255f2
RUN bitnami-pkg unpack phabricator-2018.10.0-0 --checksum ed90b7971108da5165b7f6154beb6413bb26d887792b40ae42e88248ebb1a175

COPY rootfs /

# Configure SSH for repository hosting

RUN useradd -m $PHABRICATOR_SSH_USER
RUN usermod -p '*' $PHABRICATOR_SSH_USER

RUN echo "$PHABRICATOR_SSH_USER ALL=(daemon) SETENV: NOPASSWD: /opt/bitnami/git/bin/git-upload-pack, /opt/bitnami/git/bin/git-receive-pack" >> /etc/sudoers

RUN cp /opt/bitnami/phabricator/resources/sshd/phabricator-ssh-hook.sh /usr/share/
RUN sed -i s/VCSUSER=.*/VCSUSER=\"$PHABRICATOR_SSH_USER\"/i /usr/share/phabricator-ssh-hook.sh
RUN sed -i 's|ROOT=.*|ROOT=\"/opt/bitnami/phabricator\"|i' /usr/share/phabricator-ssh-hook.sh
RUN chown root /usr/share/phabricator-ssh-hook.sh
RUN chmod 755 /usr/share/phabricator-ssh-hook.sh

RUN cat /opt/bitnami/phabricator/resources/sshd/sshd_config.phabricator.example > /etc/ssh/sshd_config
RUN sed -i 's|AuthorizedKeysCommand .*|AuthorizedKeysCommand /usr/share/phabricator-ssh-hook.sh|i' /etc/ssh/sshd_config
RUN sed -i 's/AuthorizedKeysCommandUser .*/AuthorizedKeysCommandUser '$PHABRICATOR_SSH_USER'/i' /etc/ssh/sshd_config
RUN sed -i 's/AllowUsers.*/AllowUsers '$PHABRICATOR_SSH_USER'/i' /etc/ssh/sshd_config
RUN sed -i 's/Port.*/Port '$PHABRICATOR_SSH_PORT_NUMBER'/i' /etc/ssh/sshd_config
RUN ln -s /opt/bitnami/php/bin/php /usr/bin/php

EXPOSE 80 443 22

ENTRYPOINT ["/app-entrypoint.sh"]
CMD ["/run.sh"]