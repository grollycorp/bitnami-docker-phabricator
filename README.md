# Bitnami Docker Phabricator (w/ Let's Encrypt and Git SSH Hosting)
This is a fork of [bitnami's docker-phabricator image](https://github.com/bitnami/bitnami-docker-phabricator). It combines bitnami's phabricator stack with a Let's Encrypt Proxy and pre-configured SSH repository hosting.

# Prerequisites

To run this application you need [Docker Engine](https://www.docker.com/products/docker-engine) >= `1.10.0` and [Docker Compose](https://www.docker.com/products/docker-compose) version `1.6.0` or later.

# Setup

This will setup a Bitnami Phabricator instance behind a HTTPS nginx proxy. It will request certificates for Let's Encrypt and will update them automatically. For more information, please see nginx-proxy and nginx-proxy-companion project pages.

Start by configuring your SSH installation to host on a port OTHER than 22. Port 22 will be used for SSH hosted repositories. If you wish to host your repositories on another port, skip this step and configure the `SSH_PORT` variable below to a port OTHER than 22 (i.e 2222).

Setup some variables used by the installer:

```
# Your cloned repo location. This will also serve as a data directory
export SITE_DIR=/opt/bitnami-docker-phabricator

# Your Phabricator public hostname
export SITE_HOST=myphabricator.CHANGE-ME.com

# Your email address for Let's Encrypt
export CERTIFICATES_EMAIL=certificates@CHANGE-ME.com

# Your account username
export ADMIN_USERNAME=admin

# Your default account password
export ADMIN_PASSWORD=changeme

# Your account email
export ADMIN_EMAIL=admin@example.com

# Your first name
export ADMIN_FIRSTNAME=Change

# Your last name
export ADMIN_LASTNAME=Me

# SSH user for hosted repositories
export SSH_USER=git

# SSH port for hosted repositories
export SSH_PORT=22
```

Important: The DNS address in `SITE_HOST` must resolve to the computer you´re installing.

In `CERTIFICATES_EMAIL` put an email address you check often (Let's Encrypt will warn you through it if your certificate are expiring -- just in case anything broke.

Next, clone this repository into `$SITE_DIR`:

```
git clone git@gitlab.com:grollycorp/bitnami-docker-phabricator.git $SITE_DIR
```

Then prepare the docker-compose.yml file from the template:

```
envsubst < $SITE_DIR/docker-compose.yml.template > $SITE_DIR/docker-compose.yml
```

Start containers and follow the logs. Wait until the initial setup is done (it may take several minutes for the SSL certificate to be prepared, be patient):

```
cd $SITE_DIR
docker-compose up -d
docker logs phabricator -f
docker logs proxy-companion -f
```

At this point, you should see the login page (albeit without styles) when you navigate to `https://$SITE_HOST`. If you cannot connect, double check the `proxy-companion` logs to ensure the certificate process is finished.

Set some config settings and stop the containers:

```
CONFIG="docker exec phabricator /opt/bitnami/phabricator/bin/config"

# Set base uri
$CONFIG set phabricator.base-uri https://$SITE_HOST

# Allows access to repositories only via https
$CONFIG set security.require-https 'true'

# Set SSH User
$CONFIG set diffusion.ssh-user $SSH_USER

# Set SSH Port
$CONFIG set diffusion.ssh-port $SSH_PORT

# Stop containers
docker-compose down
```

Create a `preamble.php` file in `$SITE_DIR`:

```
touch $SITE_DIR/preamble.php
```

Edit `docker-compose.yml` and add the following line in `services: > phabricator: > volumes:`

```
- ./preamble.php:/opt/bitnami/phabricator/support/preamble.php
```

Start containers:

```
docker-compose up -d
```

Create `preamble.php` file inside the container:

```
(docker exec -i phabricator bash -c "cat > /opt/bitnami/phabricator/support/preamble.php")<<'EOF'
<?php

$_SERVER['HTTPS'] = true;
EOF
```

Restart the containers

```
docker-compose down
docker-compose up -d
```

And you're done!

Important: The `preamble.php` file steps should not be taken during the initial setup or you will get an `ELOOP: too many symbolic links encountered` error. 

Access your application at <https://$SITE_HOST/>

# Upgrading Phabricator

Follow these steps to upgrade your container. Before upgrading, it is recommended that you backup your application data.

Get the updated code:

1. Pull the updated code

  ```
  $ cd $SITE_DIR && git pull
  ```

2. Stop the running containers

  ```bash
  $ docker-compose down
  ```

3. Fetch the latest images

  ```bash
  $ docker-compose pull
  ```

4. Build the updated container
  
  ```bash
  $ docker-compose build --no-cache --pull phabricator
  ```

5. Launch the updated services

  ```bash
  $ docker-compose up -d --force-recreate
  ```

# Backing up your application

To backup your application data, simply make a backup of the `$SITE_DIR/persistence` directory:

```bash
$ rsync -a $SITE_DIR/persistence /path/to/backups/phabricator.backup.$(date +%Y%m%d-%H.%M.%S)
```

# Restoring a backup

To restore your application using backed up data simply mount the backed up `persistence` data to `$SITE_DIR/persistence`.

# Special Thanks
Special thanks is given to the [bitnami team](https://bitnami.com) for providing the bulk of this setup, and to [@fdcastel](https://github.com/fdcastel) for providing Let's Encrypt support.

# License

Copyright 2016 Bitnami

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
